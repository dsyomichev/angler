"use strict";

const path = require("path");
const fs = require("fs");
const { format } = require("winston");

const { PROCESS_NAME, LOG_DIRECTORY } = require("./env");
const { logs: database } = require("./connections");

fs.existsSync(LOG_DIRECTORY) || fs.mkdirSync(LOG_DIRECTORY);

module.exports = {
  levels: {
    trace: 4,
    debug: 3,
    info: 2,
    warn: 1,
    error: 0
  },
  colors: {
    trace: "white",
    debug: "cyan",
    info: "green",
    warn: "yellow",
    error: "red"
  },
  level: "info",
  format: "json",
  silent: false,
  exitOnError: false,
  transports: {
    console: {
      level: "trace",
      format: format.combine(
        format.colorize(),
        format.timestamp({ format: "HH:mm:ss" }),
        format.align(),
        format.printf(info => {
          const { timestamp, level, message, ...extra } = info;

          return `${timestamp} [${level}]: ${message} ${
            Object.keys(extra).length ? JSON.stringify(extra, null, 2) : ""
          }`;
        })
      )
    },
    file: {
      level: "trace",
      format: format.combine(format.timestamp(), format.json()),
      filename: `${LOG_DIRECTORY}/${PROCESS_NAME}-dev.log`
    },
    rotated: {
      level: "trace",
      datePattern: "YYYY-MM-DD",
      zippedArchive: true,
      filename: `${PROCESS_NAME}-%DATE%.log`,
      format: format.combine(format.timestamp(), format.json()),
      dirname: LOG_DIRECTORY
    },
    database: {
      level: "info",
      db: database,
      options: { useNewUrlParser: true },
      collection: PROCESS_NAME,
      tryReconnect: true,
      decolorize: true
    }
  }
};
