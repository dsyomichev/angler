"use strict";

const path = require("path");

const env = process.env;

const variables = {
  // PROCESS
  NODE_ENV: env.NODE_ENV || "development",
  PROCESS_NAME: env.PROCESS_NAME || "app",

  // SERVER CONFIG
  SERVER_PORT: env.SERVER_PORT || 8080,
  SERVER_HOST: env.SERVER_HOST || "127.0.0.1",

  // DATABASE CONFIG
  DB_USERNAME: env.DB_USERNAME,
  DB_PASSWORD: env.DB_PASSWORD,
  DB_NAME: env.DB_NAME || "app",
  DB_URL: env.DB_URL || "127.0.0.1:27017",

  // LOGS
  LOG_DIRECTORY: env.LOG_DIRECTORY || path.resolve(__dirname, "../logs")
};

const check = name => {
  if (!env[name]) throw new Error(`Missing Environment Variable ${name}.`);
};

if (variables.NODE_ENV !== "development") {
  for (const key in variables) check(key);
} else {
  console.warn("\x1b[31mWARNING:\x1b[0m DEFAULT ENV ARE FOR DEVELOPMENT ONLY.\n");
}

module.exports = variables;
