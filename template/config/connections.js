"use strict";

const {
  NODE_ENV,
  SERVER_PORT,
  SERVER_HOST,
  DB_USERNAME,
  DB_PASSWORD,
  DB_NAME,
  DB_URL
} = require("./env");

const production = NODE_ENV === "production";

const database = `mongodb://${production ? `${DB_USERNAME}:${DB_PASSWORD}@` : ""}${DB_URL}`;

module.exports = {
  port: SERVER_PORT,
  host: SERVER_HOST,
  database: `${database}/${DB_NAME}`,
  logs: `${database}/logs`
};
