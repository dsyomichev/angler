"use strict";

const express = require("express");
const mongoose = require("mongoose");

const { port, host, database } = require("./config/connections");

const app = express();
require("./lib/logger")(app);

app.use("/", require("./router"));

(async () => {
  try {
    await mongoose.connect(
      database,
      { useNewUrlParser: true, useCreateIndex: true }
    );
  } catch (err) {
    if (err) throw err;
    return;
  }
  app.log.info(`Server connected to MongoDB at ${database}`);

  app.listen(port, host);
  app.log.info(`Server listening for requests on ${host}:${port}`);
})();
