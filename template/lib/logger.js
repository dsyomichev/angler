"use strict";

const path = require("path");
const winston = require("winston");
require("winston-mongodb");
require("winston-daily-rotate-file");

const production = process.env.NODE_ENV === "production";

const {
  levels,
  colors,
  level,
  format,
  silent,
  exitOnError,
  transports
} = require("../config/logs");

const logger = winston.createLogger({
  levels,
  level,
  silent,
  exitOnError,
  transports: production
    ? [
        new winston.transports.DailyRotateFile(transports.rotated),
        new winston.transports.MongoDB(transports.database)
      ]
    : [
        new winston.transports.Console(transports.console),
        new winston.transports.File(transports.file)
      ]
});

winston.addColors(colors);

logger.stream = {
  write: (msg, enc) => {
    logger.info(msg.trim());
  }
};

const middleware = app => {
  app.log = logger;
  app.use(require("morgan")(production ? "combined" : "tiny", { stream: logger.stream }));
};

module.exports = middleware;
module.exports.logger = logger;
