const router = require("express").Router();

router.get("/", (req, res) => {
  res.status(200).json({ welcome: "home" });
});

module.exports = router;
