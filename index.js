"use strict";

const fs = require("fs-extra");
const path = require("path");
const chalk = require("chalk");
const shell = require("shelljs");

module.exports = async (directory, { gitignore, npmrc, force, install }) => {
  const dir = path.isAbsolute(directory) ? directory : path.join(process.cwd(), directory);

  try {
    await fs.ensureDirSync(dir);
  } catch (err) {
    console.error(`${chalk.red("Error: ")} ${err.message}`);
    return;
  }

  try {
    let contents = fs.readdirSync(dir);
    if (contents.includes(".git")) contents.splice(contents.indexOf(".git"), 1);
    if (contents.length > 0 && !force) {
      throw new Error("Destination directory is not empty.");
    }
  } catch (err) {
    console.error(`${chalk.red("Error: ")} ${err.message}`);
    return;
  }

  const pkg = {
    name: path.basename(dir),
    version: "1.0.0",
    description: "",
    main: "app.js",
    private: true,
    author: "Striife",
    dependencies: {
      express: "^4.16.4",
      mongoose: "^5.3.4",
      morgan: "^1.9.1",
      winston: "^3.1.0",
      "winston-daily-rotate-file": "^3.3.3",
      "winston-mongodb": "^4.0.3"
    },
    devDependencies: {
      nodemon: "^1.18.5"
    }
  };

  const rc = "package-lock=false";

  const ignore = "# Logs\nlog\nlogs\n*.log\n\n# Dependency directory\nnode_modules/";

  const readme = pkg.name;

  try {
    await fs.writeJson(path.join(dir, "package.json"), pkg, { spaces: 2 });
    fs.writeFileSync(path.join(dir, "README.md"), readme, "utf-8");
    if (npmrc) fs.writeFileSync(path.join(dir, ".npmrc"), rc, "utf-8");
    if (gitignore) fs.writeFileSync(path.join(dir, ".gitignore"), ignore, "utf-8");
  } catch (err) {
    console.error(`${chalk.red("Error: ")} ${err.message}`);
    return;
  }

  try {
    await fs.copy(path.join(__dirname, "template"), dir, { overwrite: true });
  } catch (err) {
    console.error(`${chalk.red("Error: ")} ${err.message}`);
    return;
  }

  shell.cd(dir);
  if (install) shell.exec("npm install");
};
